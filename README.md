# mats-tech-blog

Sources for my tech blog

## Local test

Call ./scripts/start_in_docker.sh and open 127.0.0.1 in browser

## Light/Dark mode

Big thanks to [Derek Kedziora](https://derekkedziora.com/blog/dark-mode). I had to modify his solution, because it didn't work with the default jekyll theme. You can check my solution in the sources related to the relevant commits ([head.html](./_includes/head.html) and [style.scss](./assets/css/style.scss))
