---
layout: page
title: About me
permalink: /about/
---

I'm a professional (embedded) software engineer and hobby nerd. My focus is on real C++, hardware, clean code, software architecture,....  
Living in a beautiful swiss mountain area and always trying to learn something new.

[Why yet another blog about tech stuff?]({{ site.baseurl }}{% post_url 2019-09-17-yet-another-blog %})
