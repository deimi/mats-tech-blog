#!/bin/bash

CONTAINER_NAME=tech_blog_test

docker run -it -v $(pwd):/srv/jekyll -v jekyll_bundle:/usr/local/bundle -p 127.0.0.1:80:4000/tcp --name ${CONTAINER_NAME} jekyll/jekyll:3.8.6 jekyll serve $1

docker container rm ${CONTAINER_NAME}