---
layout: post
title:  "Yet another blog about tech stuff"
date:   2019-09-17 05:00:01 +0100
categories: 
tags: [Featured]
comment_issue_id: 2
---

You may think now "Oh man, yet another tech blog from some random guy who wants to get famous". I understand you, cause I've also thought this about other useless blogs. Even as I was thinking about writing my own blog, I had in mind how embarrassing it would be to have a blog with just a few posts and zero readers. But there are two big reasons why I still want to write a blog.



# 1. Practice explaining 
A few days ago I stumbled across an interview with Robert C. Martin (a.k.a. Uncle Bob) at [Heise Developer][heise-interview] (sorry it's only available in German, but [Micheal Keller][keller-translation] wrote a little summary in English).  
The interviewer asked Uncle Bob about some tips for developers and one of them was to write a blog. The reason for this tip is not to help you free your mind, get famous or other crap. No, the main purpose of writing such blogs should be to train your skills in explaining technical solutions and your thoughts.  
At work I sometimes recognize that I struggle to explain technical stuff which I have in my mind. When I am speaking to someone, my mind is already three steps ahead because I am so enthusiastic about the topic. So at the end the person opposite of me stares at me with a big question mark in his face. Then the discussion has to start from the beginning. Maybe a little different but definitely slower with more time for "thinking before talking" for myself.  
But not only talking about technical solutions is sometimes hard. As an engineer I also have to document my implementation. No matter if it is documented directly by the code itself, comments in the code, UML diagrams,  markdown files, etc., it is very time consuming and frustrating if you have to write several lines/paragraphs again, because they are not understandable at first try. Even during writing these lines my mind is already focusing on the next paragraph instead of this one....

Being aware of my limited "explaining complex stuff at first try"-skills, I was always wondering how I can achieve a higher level and talk understandable at first try.  
Thank you Uncle Bob, I will give it a try.

# 2. Share my knowledge
On the other hand I am a big fan of open source and my opinion is that open source should not only cover source files, schematics, etc., but also knowledge. No matter what kind of problem I have during work or hobby time, I always ask Google if someone else already came across the same problem and figured out a solution (I hate inventing the wheel again). But sometimes no matter how hard I try, I can't find any solution. Most of the time I can solve it by myself after long investigation and combination of several different solutions.  
In spirit of open source, I want to share this gained knowledge and help other with the same rare issues/ideas/....  

....and no, I will definitely not write about stuff which is already covered by other blogs (only if I have not found it with my way of using Google)

# TLDR
Main purpose of this blog is to train myself in explaining. Furthermore I want to share my knowledge about solutions for rare problems for which I have not found a solution in the web.  
I definitely don't do this for money or fame...  
  

[heise-interview]: https://www.heise.de/developer/artikel/Uncle-Bob-Nichts-geschieht-in-der-heutigen-Gesellschaft-ohne-Software-4442721.html?seite=all
[keller-translation]: https://blogs.sap.com/2019/07/07/interview-with-robert-c.-martin/